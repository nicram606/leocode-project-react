import React from 'react';
import { render } from '@testing-library/react';
import Users from '.';
import { act } from 'react-dom/test-utils';
import fetchMock from 'fetch-mock';

// This test suite contains a workaround for useEffect
// issue with testing-library.
// More can be found here: https://stackoverflow.com/questions/55181009/jest-react-testing-library-warning-update-was-not-wrapped-in-act

beforeAll(() => {
  fetchMock.mock('https://jsonplaceholder.typicode.com/users', '[]');
});

test('Does render without errors', async () => {
  let container;
  await act(async () => {
    const usersContainer = render(<Users />);
    container = usersContainer.container;
  });
  expect(container).toBeInTheDocument();
});

test('Does render header', async () => {
  let getByText;
  await act(async () => {
    const usersContainer = render(<Users />);
    getByText = usersContainer.getByText;
  });
  expect(getByText(/Users list/)).toBeInTheDocument();
});
