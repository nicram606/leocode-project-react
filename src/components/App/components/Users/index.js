import React, { useEffect, useState } from 'react';
import './Users.css';
import Header from './components/Header';
import FilteredUsers from './components/FilteredUsers';
import LoadingInProgress from './components/LoadingInProgress';
import UnableToAccessApi from './components/UnableToAccessApi';
import makeCancelable from '../../../../utils/cancelableFuture';

const reduceUser = ({ id, name, username }) => ({ id, name, username });

function Users ({ apiUrl = 'https://jsonplaceholder.typicode.com/users' }) {
  const [isLoading, setLoadingState] = useState(true);
  const [downloadError, setError] = useState(false);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Abort controller for cancelling ongoing fetch if component
    // will be unmounted during the HTTP call.
    const abortController = new AbortController();
    const signal = abortController.signal;

    let cancelableJsonParsing = null;

    // Reset the loading and error state in case useEffect's dependencies
    // are changed and called more than once.
    setError(false);
    setLoadingState(true);

    // Attempt to fetch the data.
    //
    // .finally() is not used due to possible unmounting issues during
    // HTTP call or response parsing.
    fetch(apiUrl, { signal }).then((response) => {
      if (!response.ok) {
        setError(true);
        setLoadingState(false);
        return;
      }

      cancelableJsonParsing = makeCancelable(response.json());
      cancelableJsonParsing.promise
        .then((usersData) => {
          setUsers(usersData.map(reduceUser));
          setError(false);
          setLoadingState(false);
        })
        .catch((_) => {
          setError(true);
          setLoadingState(false);
        });
    }).catch((e) => {
      if (e.name !== 'AbortError') {
        setError(true);
        setLoadingState(false);
      }
    });

    return function cleanup () {
      // Abort fetching if component is about to be unmounted.
      abortController.abort();
      if (cancelableJsonParsing) {
        cancelableJsonParsing.cancel();
      }
    };
  }, []);

  return (
    <div className='Users'>
      <Header />
      {
        isLoading && <LoadingInProgress />
      }
      {
        downloadError && <UnableToAccessApi />
      }
      {
        !isLoading && !downloadError && <FilteredUsers users={users} />
      }
    </div>
  );
}

export default Users;
