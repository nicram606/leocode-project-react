import React from 'react';
import './UnableToAccessApi.css';

function UnableToAccessApi () {
  return (
    <p className='UnableToAccessApi'>
      💥
      Unable to access the API. Try again later.
      💥
    </p>
  );
}

export default UnableToAccessApi;
