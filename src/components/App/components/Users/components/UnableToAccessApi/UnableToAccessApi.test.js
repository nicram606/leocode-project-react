import React from 'react';
import { render } from '@testing-library/react';
import UnableToAccessApi from '.';

test('Does render without errors', () => {
  const { container } = render(<UnableToAccessApi />);
  expect(container).toBeInTheDocument();
});

test('Does render the message', () => {
  const { getByText } = render(<UnableToAccessApi />);
  expect(getByText(/Unable to access the API. Try again later./)).toBeInTheDocument();
});
