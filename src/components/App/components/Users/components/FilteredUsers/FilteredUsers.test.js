import React from 'react';
import { render } from '@testing-library/react';
import FilteredUsers from '.';

test('Does render without errors', () => {
  const { container } = render(<FilteredUsers />);
  expect(container).toBeInTheDocument();
});
