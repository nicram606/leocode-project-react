import React from 'react';
import './UserRow.css';
import UserId from './components/UserId';
import UserName from './components/UserName';
import UserUsername from './components/UserUsername';

function UserRow ({ id, name, username }) {
  return (
    <div className='UserRow'>
      <UserId id={id} />
      <UserName name={name} />
      <UserUsername username={username} />
    </div>
  );
}

export default UserRow;
