import React from 'react';
import { render } from '@testing-library/react';
import NoUsersFound from '.';

test('Does render without errors', () => {
  const { container } = render(<NoUsersFound />);
  expect(container).toBeInTheDocument();
});

test('Does render the message', () => {
  const { getByText } = render(<NoUsersFound />);
  expect(getByText(/No users found/)).toBeInTheDocument();
});
