import React from 'react';
import './NoUsersFound.css';

function NoUsersFound () {
  return (
    <p className='NoUsersFound'>
      No users found
    </p>
  );
}

export default NoUsersFound;
