import React from 'react';
import './UsersList.css';
import NoUsersFound from './components/NoUsersFound';
import UserRow from './components/UserRow';

function UsersList ({ users = [] }) {
  return (
    <div className='UsersList'>
      {
        users.length === 0 && <NoUsersFound />
      }
      {
        users.map((user) => <UserRow key={user.id} {...user} />)
      }
    </div>
  );
}

export default UsersList;
