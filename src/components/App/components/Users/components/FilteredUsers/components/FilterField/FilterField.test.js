import assert from 'assert';
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import FilterField from '.';

test('Does render without errors', () => {
  const { container } = render(<FilterField />);
  expect(container).toBeInTheDocument();
});

test('Does render placeholder message', () => {
  const { getByPlaceholderText } = render(<FilterField />);
  expect(getByPlaceholderText(/Search by user name/)).toBeInTheDocument();
});

test('Does render start filter', () => {
  const startFilter = 'start';
  const { getByDisplayValue } = render(<FilterField startValue={startFilter} />);
  expect(getByDisplayValue(/start/)).toBeInTheDocument();
});

test('Does call onUsernameChanged immediately when start filter is provided', () => {
  const startFilter = 'start';
  let catchedValue = null;
  const handler = (newValue) => {
    catchedValue = newValue;
  };
  render(<FilterField startValue={startFilter} onUsernameChanged={handler} />);
  assert.strictEqual(catchedValue, startFilter);
});

test('Does call onUsernameChanged after user input', () => {
  const textToType = 'type';
  let catchedValue = null;
  const handler = (newValue) => {
    catchedValue = newValue;
  };
  const { getByLabelText } = render(<FilterField onUsernameChanged={handler} />);
  const input = getByLabelText('filter field');
  fireEvent.change(input, { target: { value: textToType } });
  assert.strictEqual(catchedValue, textToType);
});
