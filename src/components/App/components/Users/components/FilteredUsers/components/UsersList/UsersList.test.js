import React from 'react';
import { render, getAllByText } from '@testing-library/react';
import UsersList from '.';

test('Does render without errors', () => {
  const { container } = render(<UsersList />);
  expect(container).toBeInTheDocument();
});

test('Does render no users message when users prop is not provided', () => {
  const { getByText } = render(<UsersList />);
  expect(getByText(/No users found/)).toBeInTheDocument();
});

test('Does render no users message when users prop is empty', () => {
  const { getByText } = render(<UsersList users={[]} />);
  expect(getByText(/No users found/)).toBeInTheDocument();
});

test('Does render container for each provided user', () => {
  const { getAllByText } = render(<UsersList users={[{ id: 1 }, { id: 2 }, { id: 3 }]} />);
  expect(getAllByText('Unknown')).toHaveLength(3);
});
