import React from 'react';
import { render } from '@testing-library/react';
import UserUsername from '.';

test('Does render without errors', () => {
  const { container } = render(<UserUsername />);
  expect(container).toBeInTheDocument();
});

test('Does render default unknown if name is not provided', () => {
  const { getByText } = render(<UserUsername />);
  expect(getByText(/unknown/)).toBeInTheDocument();
});

test('Does not render @ for default unknown if name is not provided', () => {
  const { queryByText } = render(<UserUsername />);
  expect(queryByText(/@/)).toBeNull();
});

test('Does render provided username with an @', () => {
  const testName = 'testusername';
  const { getByText } = render(<UserUsername username={testName} />);
  expect(getByText(`@${testName}`)).toBeInTheDocument();
});
