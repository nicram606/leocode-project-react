import React, { useState, useEffect } from 'react';
import './FilterField.css';

function FilterField ({ startValue = '', onUsernameChanged }) {
  const [value, setValue] = useState(startValue);

  const handleNewValue = (newValue) => {
    setValue(newValue);
    if (onUsernameChanged) {
      onUsernameChanged(newValue);
    }
  };

  useEffect(() => {
    if (onUsernameChanged) {
      onUsernameChanged(startValue);
    }
  }, [startValue]);

  return (
    <input
      className='FilterField'
      aria-label='filter field'
      placeholder='Search by user name...'
      value={value}
      onChange={(event) => handleNewValue(event.target.value)}
    />
  );
}

export default FilterField;
