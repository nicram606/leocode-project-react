import React from 'react';
import { render } from '@testing-library/react';
import UserRow from '.';

test('Does render without errors', () => {
  const { container } = render(<UserRow />);
  expect(container).toBeInTheDocument();
});
