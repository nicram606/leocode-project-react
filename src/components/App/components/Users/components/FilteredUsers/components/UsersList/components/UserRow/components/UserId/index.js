import React from 'react';
import './UserId.css';

function UserId ({ id = '?' }) {
  return (
    <span className='UserId'>
      {id}.
    </span>
  );
}

export default UserId;
