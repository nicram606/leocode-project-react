import React from 'react';
import './UserName.css';

function UserName ({ name = 'Unknown' }) {
  return (
    <span className='UserName'>
      {name}
    </span>
  );
}

export default UserName;
