import React from 'react';
import { render } from '@testing-library/react';
import UserName from '.';

test('Does render without errors', () => {
  const { container } = render(<UserName />);
  expect(container).toBeInTheDocument();
});

test('Does render default Unknown if name is not provided', () => {
  const { getByText } = render(<UserName />);
  expect(getByText(/Unknown/)).toBeInTheDocument();
});

test('Does render provided name', () => {
  const testName = 'Test Name';
  const { getByText } = render(<UserName name={testName} />);
  expect(getByText(testName)).toBeInTheDocument();
});
