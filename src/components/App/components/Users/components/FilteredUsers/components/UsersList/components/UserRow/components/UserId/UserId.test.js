import React from 'react';
import { render } from '@testing-library/react';
import UserId from '.';

test('Does render without errors', () => {
  const { container } = render(<UserId />);
  expect(container).toBeInTheDocument();
});

test('Does render default ? if id is not provided', () => {
  const { getByText } = render(<UserId />);
  expect(getByText(/\?/)).toBeInTheDocument();
});

test('Does render provided id with a dot', () => {
  const testId = 321;
  const { getByText } = render(<UserId id={testId} />);
  expect(getByText(`${testId}.`)).toBeInTheDocument();
});
