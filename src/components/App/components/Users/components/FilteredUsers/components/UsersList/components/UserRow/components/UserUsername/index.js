import React from 'react';
import './UserUsername.css';

function UserUsername ({ username }) {
  return (
    <span className='UserUsername'>
      {
        username ? `@${username}` : 'unknown'
      }
    </span>
  );
}

export default UserUsername;
