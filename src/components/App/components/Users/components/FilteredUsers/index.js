import React, { useState } from 'react';
import './FilteredUsers.css';
import FilterField from './components/FilterField';
import UsersList from './components/UsersList';

function FilteredUsers ({ users = [] }) {
  const [filter, setFilter] = useState('');

  const handleNewFilter = (newFilter) => {
    setFilter(newFilter);
  };

  return (
    <div className='FilteredUsers'>
      <FilterField onUsernameChanged={handleNewFilter} />
      <UsersList users={
        filter.length > 0
          ? users.filter((user) => user.name.includes(filter))
          : users
      }
      />
    </div>
  );
}

export default FilteredUsers;
