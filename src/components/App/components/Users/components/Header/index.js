import React from 'react';
import './Header.css';

function Header () {
  return (
    <h1 className='Header'>Users list</h1>
  );
}

export default Header;
