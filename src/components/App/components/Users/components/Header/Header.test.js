import React from 'react';
import { render } from '@testing-library/react';
import Header from '.';

test('Does render without errors', () => {
  const { container } = render(<Header />);
  expect(container).toBeInTheDocument();
});

test('Does render the message', () => {
  const { getByText } = render(<Header />);
  expect(getByText(/Users list/)).toBeInTheDocument();
});
