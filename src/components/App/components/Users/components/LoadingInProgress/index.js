import React from 'react';
import './LoadingInProgress.css';

function LoadingInProgress () {
  return (
    <p className='LoadingInProgress'>
      Loading...
    </p>
  );
}

export default LoadingInProgress;
