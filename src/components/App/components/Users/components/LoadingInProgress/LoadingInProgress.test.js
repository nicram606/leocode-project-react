import React from 'react';
import { render } from '@testing-library/react';
import LoadingInProgress from '.';

test('Does render without errors', () => {
  const { container } = render(<LoadingInProgress />);
  expect(container).toBeInTheDocument();
});

test('Does render the message', () => {
  const { getByText } = render(<LoadingInProgress />);
  expect(getByText(/Loading/)).toBeInTheDocument();
});
