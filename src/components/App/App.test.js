import React from 'react';
import { render } from '@testing-library/react';
import App from '.';

test('Does render without errors', () => {
  const { container } = render(<App />);
  expect(container).toBeInTheDocument();
});
